import { CounterRepository } from "../domain/counter";

const CounterRepositoryMock = (): CounterRepository => ({
    async loadCounter() {
        return Promise.resolve(0);
    }
});

export default CounterRepositoryMock;

