import { CounterRepository } from "../domain/counter";
import * as ApiService from '../../foundation/infrastructure/api-service';

type Dependencies = {
    apiService: typeof ApiService
}

const CounterRepositoryApi = ({ apiService }: Dependencies): CounterRepository => ({
    async loadCounter() {
        return ApiService.get('counter')
    }
});

export default CounterRepositoryApi;
