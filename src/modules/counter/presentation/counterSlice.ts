import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../../store";
import decrementCounterUseCase from "../application/decrement-counter-use-case";
import incrementCounterUseCase from "../application/increment-counter-use-case";
import loadCounterUseCase from "../application/load-counter-use-case";
import { Counter, create } from "../domain/counter";
import CounterRepositoryMock from "../infrastructure/counter-repository-mock";

export interface CounterState {
  entity: Counter;
  status: "idle" | "loading" | "failed";
}

const initialState: CounterState = {
  entity: create(0),
  status: "idle",
};

const useCases = {
  increment: incrementCounterUseCase(),
  decrement: decrementCounterUseCase(),
  load: loadCounterUseCase({ counterRepository: CounterRepositoryMock() }),
};

export const counterSlice = createSlice({
  name: "counter",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    increment: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.entity = useCases.increment(state.entity);
    },
    decrement: (state) => {
      state.entity = useCases.decrement(state.entity);
    },
  },
});

export const { increment, decrement } = counterSlice.actions;


// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectCount = (state: RootState) => state.counter.entity;

export default counterSlice.reducer;
