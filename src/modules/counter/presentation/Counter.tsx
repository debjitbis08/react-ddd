import * as React from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { show } from "../domain/counter";
import {
  decrement,
  increment,
  selectCount,
} from './counterSlice';

const Counter = () => {
    const count = useAppSelector(selectCount);
    const dispatch = useAppDispatch();

    return (
        <div className="counter">
            <button onClick={() => dispatch(decrement())}>-</button>
            <span>{show(count)}</span>
            <button onClick={() => dispatch(increment())}>+</button>
        </div>
    )
};

export default Counter;
