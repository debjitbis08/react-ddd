import { CounterRepository, create } from "../domain/counter";

type Dependencies = {
    counterRepository: CounterRepository;
}

const loadCounterUseCase = ({ counterRepository }: Dependencies) => {
    return async () => {
        const counterValue = await counterRepository.loadCounter();
        return create(counterValue);
    };
};

export default loadCounterUseCase;
