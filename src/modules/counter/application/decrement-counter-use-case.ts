import { Counter, decrement } from "../domain/counter";

const decrementCounterUseCase = () => {
    return (counter: Counter) => {
        return decrement(counter)
    };
};

export default decrementCounterUseCase;
