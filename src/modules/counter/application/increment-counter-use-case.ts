import { Counter, increment } from "../domain/counter";

const incrementCounterUseCase = () => {
    return (counter: Counter) => {
        return increment(counter)
    };
};

export default incrementCounterUseCase;
