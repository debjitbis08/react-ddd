/**
 * Entity file with data structure definition and behaviour.
 */

export interface Counter {
    value: number
}

export const create = (value: number): Counter => ({ value });
export const increment = (counter: Counter): Counter => ({ value: counter.value + 1 });
export const decrement = (counter: Counter): Counter => ({ value: Math.max(counter.value - 1, 0) });
export const show = (counter: Counter): string => counter.value.toString();

export interface CounterRepository {
    loadCounter: () => Promise<number>
}
